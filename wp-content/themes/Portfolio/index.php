<?php get_header(); ?>
<?php  get_posts();?>
<?php get_comments(); ?>
<div class="titelbalk">
    <h1 class="entry-title">Posts</h1>
</div>
<div class="container">


        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <div class="posts">
                    <h3 class="entrytitle" id="post-<?php the_ID(); ?>"> <?php the_title(); ?>  </h3>
                    <p>
                        <img src="http://robbvand3.byethost32.com/wp-content/themes/Portfolio/content/images/massage.png">
                        <?php comments_number( '' ); ?>.
                    </p>
                    <?php the_content(); ?>
                    <a href="<?php the_permalink() ?>" rel="bookmark"> Lees meer</a>
                </div>
            <?php endwhile; ?>
        <?php else : ?>
            <h6 class="center">Not Found</h6>
            <p class="center">Sorry, but you are looking for something that isn't here.</p>
            <?php include (TEMPLATEPATH . "/searchform.php"); ?>
        <?php endif; ?>

</div>

<?php get_footer(); ?>